import numpy as np
import matplotlib.pyplot as plt

mu = [0.95, 0.4, 0.2, 0.153, 0.125, 0.1, 0.09, 0.08, 0.068, 0.0451, 0.041, 0.04]
mu = np.array(mu)
rho = 11.94
y = 1/np.exp(-mu*rho*3)
x = np.array([0.2 + i/10 for i in range(9)] + [2,3,4])
print(x)

plt.figure(figsize=(8, 6))

plt.xlabel("Énergie du gamma (MeV)", size = 16,)
plt.ylabel("Atténuation", size = 16)
plt.title("Atténuation en fonction de l'énergie des gamma")

plt.plot(x,y, marker = 'o', linestyle="None")
plt.xscale("log")
plt.yscale("log")
plt.show()
